import os
import json


# define global identifiers
ROOT_DIR = './IBAtS_Initial_Dataset/'


def standardise_gold_tagging(filepath):
    '''Remove bogus tags'''

    with open(filepath, 'r+') as json_file:
        detections = json.load(json_file)
        bogus_tags = []
        for idx, tag in enumerate(detections):
            if tag['width'] <= 0 or tag['height'] <= 0:
                print('Bogus Tag Detected')
                bogus_tags.append(idx)
                continue

            # tag['color'] = '#00ff00'
            # convert float to integer
            tag['x'] = int(tag['x'])
            tag['y'] = int(tag['y'])
            tag['width'] = int(tag['width'])
            tag['height'] = int(tag['height'])

        # Remove bogus tags
        if len(bogus_tags) > 0:
            bogus_tags = set(bogus_tags)
            clean_detections = [tag for idx, tag in enumerate(
                detections) if idx not in bogus_tags]
            detections = clean_detections

        # write updated json into json file
        json_file.seek(0)   # reset file position to the beginning.
        json.dump(obj=detections, fp=json_file, indent=4)
        json_file.truncate()     # remove remaining part


def main():
    '''main function'''
    classrooms = os.listdir(ROOT_DIR)
    for classroom in classrooms:
        dates = os.listdir(os.path.join(ROOT_DIR, classroom))
        for date in dates:
            filenames = os.listdir(os.path.join(ROOT_DIR, classroom, date))
            for filename in filenames:
                if '.gold.json' not in filename:
                    continue
                filepath = os.path.join(ROOT_DIR, classroom, date, filename)
                standardise_gold_tagging(filepath)


if __name__ == "__main__":
    main()
