import os
import csv
import json
from IBAtS_global import ROOT_DIR, CLASSIFIERS, load_json


def create_summary_table(filepath):
    '''Create a summary table (csv format) based on the evaluation files (json format)'''
    toCSV = []
    fnames = os.listdir(filepath)
    for fname in fnames:
        if not fname.startswith('[resize]') or not fname.endswith('.eval.json'):
            continue  # skip files other than evaluation files

        eval_json = load_json(os.path.join(filepath, fname))
        toCSV.append({'classifier': eval_json['classifier'],
                      'true_positive': eval_json['true_positive'],
                      'false_positive': eval_json['false_positive'],
                      'false_negative': eval_json['false_negative']})

    keys = toCSV[0].keys()
    with open('evaluation_summary.csv', 'a') as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        if os.stat('evaluation_summary.csv').st_size == 0:
            dict_writer.writeheader()
        dict_writer.writerows(toCSV)


def main():
    '''main function'''
    # THE SHORT CODE BELOW FOR RUN THE SCRIPT IN SMALLER SCOPE
    # filepath = './IBAtS_Initial_Dataset/BS301/2017_09_04/'
    # evaluate_detections(filepath=filepath)

    with open('./evaluation_summary.csv', 'w') as my_empty_csv:
        pass  # create an empty csv file

    classrooms = os.listdir(ROOT_DIR)
    for classroom in classrooms:
        dates = os.listdir(os.path.join(ROOT_DIR, classroom))
        for date in dates:
            filepath = os.path.join(ROOT_DIR, classroom, date)
            create_summary_table(filepath=filepath)


if __name__ == "__main__":
    main()
