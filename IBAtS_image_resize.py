# import modules
import cv2
import os
from IBAtS_global import ROOT_DIR


def resize_image(filepath, max_axis=1024):
    '''Resize and save the resized image in the same directory.'''

    # load image file
    img = cv2.imread(filepath)

    # get image width and iamge height
    height, width = img.shape[:2]
    dim_long = max(width, height)

    # calculate new image width and image height
    r = max_axis / dim_long
    dim_resize = (0, 0)
    if dim_long == width:
        dim_resize = (max_axis, int(r * height))
    else:
        dim_resize = (int(r * width), max_axis)

    # resizing image
    img_resized = cv2.resize(img, dim_resize, interpolation=cv2.INTER_AREA)

    # save the resized image
    pathlist = filepath.split('/')
    filename = '[resize]{a}'.format(a=pathlist[-1])
    filename = os.path.join(*pathlist[:-1], filename)
    cv2.imwrite(filename=filename, img=img_resized)


def main():
    '''main function'''
    classrooms = os.listdir(ROOT_DIR)
    for classroom in classrooms:
        dates = os.listdir(os.path.join(ROOT_DIR, classroom))
        for date in dates:
            filenames = os.listdir(os.path.join(ROOT_DIR, classroom, date))
            for filename in filenames:
                if '.jpg' not in filename or '[resize]' in filename:
                    continue
                filepath = os.path.join(ROOT_DIR, classroom, date, filename)
                resize_image(filepath=filepath)


if __name__ == "__main__":
    main()
