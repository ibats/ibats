# import OpenCV library
import cv2
# import matplotlib library
import matplotlib.pyplot as plt
# importing os library for retieving list of files in a directory
import os

# Define global identifiers
DIR_INPUT = './photos'
DIR_OUTPUT = './HAAR_eyeface_results/'
CASCADE_DIR = './opencv/haarcascades/'
FACE_CLSF = 'haarcascade_frontalface_alt.xml'
EYE_CLSF = 'haarcascade_eye.xml'


def detect_faces(f_cascade, e_cascade, color_img, scaleFactor=1.1):
    '''Detect faces on a given image using Cascades HAAR classifier'''
    # Make a copy of image passed (avoid aliasing)
    img_copy = color_img.copy()

    # convert the test image to gray image
    # as opencv face detector expects gray images
    gray_img = cv2.cvtColor(src=img_copy, code=cv2.COLOR_BGR2GRAY)

    # Detect multiscale images
    # Some images may be closer to camera than others
    faces = f_cascade.detectMultiScale(image=gray_img,
                                       scaleFactor=scaleFactor,
                                       minNeighbors=5)

    for (x, y, w, h) in faces:
        cv2.rectangle(img_copy, (x, y), (x + w, y + h), (0, 255, 0), 2)

        roi_gray = gray_img[y:y + h, x:x + w]  # crop face area
        eyes = e_cascade.detectMultiScale(image=roi_gray)
        # order by x axis (left -> right)
        eyes = sorted(eyes, key=lambda x: x[0])
        for (ex, ey, ew, eh) in eyes:
            ex += x
            ey += y
            # ex, ey is relative position in face
            cv2.rectangle(img=img_copy, pt1=(ex, ey), pt2=(ex + ew, ey + eh),
                          color=(0, 0, 255), thickness=2)

    return img_copy


def main():
    # load cascade classifier training file for haarcascade
    haar_face_cascade = cv2.CascadeClassifier(os.path.join(CASCADE_DIR,
                                                           FACE_CLSF))
    haar_eye_cascade = cv2.CascadeClassifier(os.path.join(CASCADE_DIR,
                                                          EYE_CLSF))
    old_HAAR = os.listdir(DIR_OUTPUT)
    old_HAAR = [img_HAAR[9:] for img_HAAR in old_HAAR]

    counter = len(old_HAAR)

    for img_file in os.listdir(DIR_INPUT):
        if img_file in old_HAAR:  # skip preprocessed images
            print('Skipping image {fname}'.format(fname=img_file))
            continue

        # load another image
        print('Processing image {fname}'.format(fname=img_file))
        img = cv2.imread(os.path.join(DIR_INPUT, img_file))

        # call our function to detect faces
        faces_detected_img = detect_faces(f_cascade=haar_face_cascade,
                                          e_cascade=haar_eye_cascade,
                                          color_img=img)

        # convert image to RGB and show image
        plt.figure(figsize=(20, 20))
        fig1 = plt.gcf()
        plt.imshow(cv2.cvtColor(src=faces_detected_img,
                                code=cv2.COLOR_BGR2RGB))
        counter += 1
        img_file = 'HAAR_{ctr:03d}_{fname}'.format(ctr=counter,
                                                   fname=img_file)
        fig1.savefig(os.path.join(DIR_OUTPUT, img_file), dpi=200)


if __name__ == "__main__":
    main()
