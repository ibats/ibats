---
title: "Computer vision application in recording students attendance [An exploratory study]"
author: "Setia Budi [ORCID:0000-0003-1408-7808](http://orcid.org/0000-0003-1408-7808)"
date: "October 18, 2017"
output: 
  ioslides_presentation: default
  slidy_presentation: default
  beamer_presentation: default
autosize: yes
bibliography: bibliography.bib
csl: ieee.csl
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Motivation in Recording Students Attendance

According to [@macfarlane_surveillance_2013] there are three main motivation in recording students attendance:

- Accountability
    + Stakeholder
    + Compliance
- Student well-being
    + Academic performance
    + Student care
- Work preparation
    + Real world
    + Professional practice

## Conventional Attendance Systems

There are two conventional and commonly applied methods in tracking and recording student attendance in a class [@pss_rfid_2016;@noor_android-based_2015]:

- Roll Call
- Attendance Sheet

## State of the Art in Recording Students Attendance

- RFID based attendance systems [@silva_automatic_2008;@qaiser_automation_2006;@kassim_web-based_2012;@chang_smart_2011]
- Client-server architecture model [@mattam_architecture_2012]
- Smartphone based attendance system [@noor_android-based_2015;@soewito_attendance_2015;@anwar_design_2015;@cisar_smartphone_2016;@noguchi_student_2015]
- Voiceprint recognition based attendance system [@sweetlin_speech_2016;@yang_automated_2016]
- Face recognition based attendance system [@lukas_student_2016;@rekha_efficient_2017;@shehu_using_2010;@sajid_conceptual_2014]

## Our Work

- We are interested to explore and to replicate the implementation of face detection algorithm in lab environment.
- Key research questions:
    + Which face detection technique is the best for our lab environment?
    + What are the key parameters which influence face detection performance?
    + How to improve detection rate?

## Premilimary Data Collection

![](./prelminary_result.png)

## Building a "Gold Standard Model""

- Manual correction to our preliminary face detection results.
- Such dataset will be used as gold standard model for our exploration.

![](./manual_tagger.png)

## Finding a Suitable Technique

- For each technique, we evaluate:
    + False Positive and False Negative detections
    + Precission and Recall
- Parameter fine-tunning:
    + Scale Factor
    + Detection Threshold

## Collaborators

- Commonwealth Scientific and Idustrial Research Organisation
- University of Tasmania
- Indian Institute of Technology Guwahati

<style>
slides > slide { overflow: scroll; }
slides > slide:not(.nobackground):after {
  content: '';
}
</style>

## References

