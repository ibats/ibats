import os
import json


# define global identifiers
ROOT_DIR = './IBAtS_Initial_Dataset/'


def add_detection_central_point(filepath):
    '''Calculate and add central point for each detection tag'''

    with open(filepath, 'r+') as json_file:
        detections = json.load(json_file)
        for tag in detections:
            # standardise x, y, width, and height values to integer
            tag['x'] = int(tag['x'])
            tag['y'] = int(tag['y'])
            tag['width'] = int(tag['width'])
            tag['height'] = int(tag['height'])

            # calculate ceter points for each detection tag
            tag['x_center'] = int(tag['x'] + tag['width'] // 2)
            tag['y_center'] = int(tag['y'] + tag['height'] // 2)

        # write updated json into json file
        json_file.seek(0)   # reset file position to the beginning.
        json.dump(obj=detections, fp=json_file, indent=4)
        json_file.truncate()     # remove remaining part


def main():
    '''main function'''
    # THE SHORT CODE BELOW FOR RUN THE SCRIPT IN SMALLER SCOPE
    # dirpath = './IBAtS_Initial_Dataset/BS301/2017_09_04/'
    # filenames = os.listdir(dirpath)
    # for filename in filenames:
    #     if '.json' not in filename:
    #         continue
    #     filepath = os.path.join(dirpath, filename)
    #     add_detection_central_point(filepath)

    classrooms = os.listdir(ROOT_DIR)
    for classroom in classrooms:
        dates = os.listdir(os.path.join(ROOT_DIR, classroom))
        for date in dates:
            filenames = os.listdir(os.path.join(ROOT_DIR, classroom, date))
            for filename in filenames:
                if '.json' not in filename:
                    continue
                filepath = os.path.join(ROOT_DIR, classroom, date, filename)
                add_detection_central_point(filepath)


if __name__ == "__main__":
    main()
